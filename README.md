# boulzy/array-comparator


[![pipeline status](https://gitlab.com/boulzy/array-comparator/badges/main/pipeline.svg)](https://gitlab.com/boulzy/array-comparator/-/commits/main)
[![coverage report](https://gitlab.com/boulzy/array-comparator/badges/main/coverage.svg)](https://gitlab.com/boulzy/array-comparator/-/commits/main)
[![License](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)

## Description

The `boulzy/array-comparator` PHP library is an extraction of the `ArrayContainsComparator` originally found in
[`imbo/behat-api-extension`](https://github.com/imbo/behat-api-extension). It is designed to simplify array comparison
using matching functions, providing a set of tools to make array comparisons more efficient and convenient.

## Installation

To install this library, you can use [Composer](https://getcomposer.org):

```
composer require boulzy/array-comparator
```

## Usage

Here's how you can use this library to compare arrays using matching functions:

```php
require_once 'vendor/autoload.php';

$comparator = new \Boulzy\ArrayComparator\ArrayComparator();
$result = $comparator->compare($expected, $actual);
```

You can pass an array and/or a json string as either of the two parameters.

To see how to format the jsons / arrays, please refer to
[imbo/behat-api-extension documentation](https://behat-api-extension.readthedocs.io/en/latest/guide/verify-server-response.html#then-the-response-body-contains-json-pystringnode).

## License

This project is licensed under the [MIT License](LICENSE).
