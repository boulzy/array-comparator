<?php

declare(strict_types=1);

namespace Boulzy\ArrayComparator;

use Boulzy\ArrayComparator\Exception\ArrayComparatorException;

/**
 * Comparator class used for the response body comparisons.
 */
class ArrayComparator
{
    /**
     * @var array<string,callable>
     */
    private array $functions = [];

    /**
     * @param array<string,callable> $functions
     */
    public function __construct(array $functions = [])
    {
        $defaultFunctions = [
            'arrayLength' => new Matcher\ArrayLength(),
            'arrayMinLength' => new Matcher\ArrayMinLength(),
            'arrayMaxLength' => new Matcher\ArrayMaxLength(),
            'variableType' => new Matcher\VariableType(),
            'regExp' => new Matcher\RegExp(),
            'lt' => new Matcher\LessThan(),
            'gt' => new Matcher\GreaterThan(),
        ];

        /** @var array<string,callable> $functions */
        $functions = [...$defaultFunctions, ...$functions];
        foreach ($functions as $functionName => $function) {
            $this->addFunction($functionName, $function);
        }
    }

    /**
     * Add a custom matcher function.
     *
     * If an existing function exists with the same name it will be replaced
     */
    public function addFunction(string $name, callable $callback): self
    {
        $this->functions[$name] = $callback;

        return $this;
    }

    /**
     * Get a matcher function by name.
     */
    public function getMatcherFunction(string $name): callable
    {
        if (!isset($this->functions[$name])) {
            throw new \InvalidArgumentException(sprintf('No matcher function registered for "%s".', $name));
        }

        return $this->functions[$name];
    }

    /**
     * Recursively loop over the $haystack array and make sure all the items in $needle exists.
     * You can pass a json instead of an array, that will be decoded before running the comparison.
     *
     * To clarify, the method (and other methods in the class) refers to "lists" and "objects". A
     * "list" is a numerically indexed array, and an "object" is an associative array.
     *
     * @param array<scalar|array<scalar>>|string $expected The needle
     * @param array<mixed>|string                $actual   The haystack
     */
    public function compare(array|string $expected, array|string $actual): bool
    {
        if (\is_string($expected)) {
            $expected = \trim($expected);
            $expectedIsList = '[' === $expected[0];
            if (null === $expected = \json_decode($expected, true)) {
                throw new ArrayComparatorException('Argument $expected is not a valid JSON');
            }
        } else {
            $expectedIsList = $this->arrayIsList($expected);
        }

        if (\is_string($actual)) {
            $actual = \trim($actual);
            $actualIsList = '[' === $actual[0];
            if (null === $actual = \json_decode($actual, true)) {
                throw new ArrayComparatorException('Argument $actual is not a valid JSON');
            }
        } else {
            $actualIsList = $this->arrayIsList($actual);
        }

        // If the needle is a numerically indexed array, the haystack needs to be one as well
        if ($expectedIsList && !$actualIsList) {
            throw new ArrayComparatorException('The needle is a list, while the haystack is not.', 0, null, $expected, $actual);
        } elseif ($expectedIsList) {
            // Both arrays are numerically indexed arrays
            return $this->inArray($expected, $actual);
        }

        // Loop over all key => value pairs in the needle array and make sure they match the
        // haystack array
        foreach ($expected as $key => $value) {
            // Check if the needle key refers to a specific array key in the haystack
            $match = [];

            if (preg_match('/^(?<key>.*?)\[(?<index>[\d]+)\]$/', (string) $key, $match)) {
                $realKey = $match['key'];
                $index = (int) $match['index'];

                if (!empty($realKey) && !array_key_exists($realKey, $actual)) {
                    // The key does not exist in the haystack
                    throw new ArrayComparatorException(sprintf('Haystack object is missing the "%s" key.', $realKey), 0, null, $expected, $actual);
                }

                // If a key has been specified, use that part of the haystack to compare against,
                // if no key exists, simply use the haystack as-is.

                $subHaystack = $realKey ? $actual[$realKey] : $actual;

                if (!is_array($subHaystack) || !$this->arrayIsList($subHaystack)) {
                    // The sub haystack is not a list, so we can't really target indexes
                    throw new ArrayComparatorException(sprintf('The element at key "%s" in the haystack object is not a list.', $realKey), 0, null, $expected, $actual);
                } elseif (!array_key_exists($index, $subHaystack)) {
                    // The index does not exist in the haystack
                    throw new ArrayComparatorException(sprintf('The index "%d" does not exist in the haystack list.', $index), 0, null, $expected, $actual);
                }

                if (is_array($value)) {
                    if (!is_array($subHaystack[$index])) {
                        throw new ArrayComparatorException(sprintf('Expected haystack element on index "%d" to be an array, got "%s".', $index, gettype($subHaystack[$index])), 0, null, $expected, $actual);
                    }

                    // The value is an array, do a recursive check
                    $this->compare($value, $subHaystack[$index]);
                } elseif (!$this->compareValues($value, $subHaystack[$index])) {
                    // Comparison of values failed
                    throw new ArrayComparatorException(sprintf('Value mismatch for index "%d" in haystack list.', $index), 0, null, $value, $subHaystack[$index]);
                }
            } else {
                // Use array_key_exists instead of isset as the value of the key can be null, which
                // causes isset to return false
                if (!array_key_exists($key, $actual)) {
                    // The key does not exist in the haystack
                    throw new ArrayComparatorException(sprintf('Haystack object is missing the "%s" key.', $key), 0, null, $expected, $actual);
                }

                if (is_array($value)) {
                    if (!is_array($actual[$key])) {
                        throw new ArrayComparatorException(sprintf('Expected haystack value with key "%s" to be an array, got "%s".', $key, gettype($actual[$key])), 0, null, $expected, $actual);
                    }

                    // If the value is an array, recurse
                    $this->compare($value, $actual[$key]);
                } elseif (!$this->compareValues($value, $actual[$key])) {
                    // Comparison of values failed
                    throw new ArrayComparatorException(sprintf('Value mismatch for key "%s" in haystack object.', $key), 0, null, $expected, $actual);
                }
            }
        }

        return true;
    }

    /**
     * Compare a value from a needle with a value from the haystack.
     *
     * Based on the value of the needle, this method will perform a regular value comparison, or a
     * custom function match.
     */
    protected function compareValues(mixed $needleValue, mixed $haystackValue): bool
    {
        $match = [];

        // List of available function names
        $functions = array_map(
            fn (string $value): string => preg_quote($value, '/'),
            array_keys($this->functions),
        );

        // Dynamic pattern, based on the keys in the functions list
        $pattern = sprintf(
            '/^@(?<function>%s)\((?<params>.*?)\)$/',
            implode('|', $functions),
        );

        if (is_string($needleValue) && $functions && preg_match($pattern, $needleValue, $match)) {
            // Custom function matching
            $function = $match['function'];
            $params = $match['params'];

            try {
                $this->functions[$function]($haystackValue, $params);

                return true;
            } catch (\Exception $e) {
                throw new ArrayComparatorException(sprintf('Function "%s" failed with error message: "%s".', $function, $e->getMessage()), 0, $e, $needleValue, $haystackValue);
            }
        }

        // Regular value matching
        return $needleValue === $haystackValue;
    }

    /**
     * Make sure all values in the $needle array is present in the $haystack array.
     *
     * @param array<array<scalar>|scalar> $needle
     * @param array<array<scalar>|scalar> $haystack
     */
    protected function inArray(array $needle, array $haystack): bool
    {
        // Loop over all the values in the needle array, and make sure each and every one is in some
        // way present in the haystack, in a recursive manner.
        foreach ($needle as $needleValue) {
            if (is_array($needleValue)) {
                // If the value is an array we need to do a recursive compare / inArray check
                if ($this->arrayIsList($needleValue)) {
                    // The needle value is a list, so we only want to compare it to lists in the
                    // haystack
                    $listElementsInHaystack = array_filter(
                        $haystack,
                        fn (mixed $element): bool => is_array($element) && $this->arrayIsList($element),
                    );

                    if (empty($listElementsInHaystack)) {
                        throw new ArrayComparatorException('Haystack does not contain any list elements, needle can\'t be found.', 0, null, $needleValue, $haystack);
                    }

                    $result = array_filter($listElementsInHaystack, function (array $haystackListElement) use ($needleValue): bool {
                        try {
                            return $this->inArray($needleValue, $haystackListElement);
                        } catch (ArrayComparatorException $e) {
                            // If any error occurs, swallow it and return false to mark this as a
                            // failure
                            return false;
                        }
                    });

                    // Result is empty, which means the needle was not found in the haystack
                    if (empty($result)) {
                        throw new ArrayComparatorException('The list in needle was not found in the list elements in the haystack.', 0, null, $needleValue, $haystack);
                    }
                } else {
                    // The needle value is an object, so we only want to compare it to objects in
                    // the haystack
                    $objectElementsInHaystack = array_filter(
                        $haystack,
                        fn (mixed $element): bool => is_array($element) && $this->arrayIsObject($element),
                    );

                    if (empty($objectElementsInHaystack)) {
                        throw new ArrayComparatorException('Haystack does not contain any object elements, needle can\'t be found.', 0, null, $needleValue, $haystack);
                    }

                    $result = array_filter($objectElementsInHaystack, function (array $haystackObjectElement) use ($needleValue) {
                        try {
                            return $this->compare($needleValue, $haystackObjectElement);
                        } catch (ArrayComparatorException $e) {
                            // If any error occurs, swallow it and return false to mark this as a
                            // failure
                            return false;
                        }
                    });

                    // Result is empty, which means the needle was not found in the haystack
                    if (empty($result)) {
                        throw new ArrayComparatorException('The object in needle was not found in the object elements in the haystack.', 0, null, $needleValue, $haystack);
                    }
                }
            } else {
                $result = array_map(
                    fn (mixed $haystackElement): bool => $this->compareValues($needleValue, $haystackElement),
                    $haystack,
                );

                if (empty(array_filter($result))) {
                    throw new ArrayComparatorException('Needle is not present in the haystack.', 0, null, $needleValue, $haystack);
                }
            }
        }

        // All's right with the world!
        return true;
    }

    /**
     * See if a PHP array is a JSON array.
     *
     * @param array<mixed> $array
     */
    protected function arrayIsList(array $array): bool
    {
        $encoded = json_encode($array);

        return false !== $encoded && '[' === $encoded[0];
    }

    /**
     * See if a PHP array is a JSON object.
     *
     * @param array<scalar> $array
     */
    protected function arrayIsObject(array $array): bool
    {
        $encoded = json_encode($array);

        return false !== $encoded && '{' === $encoded[0];
    }
}
